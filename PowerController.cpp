// PowerController.cpp
//
// SPDX-License-Identifier: MIT
//
#include <Arduino.h>
#include <Arduino_FreeRTOS.h> // https://github.com/feilipu/Arduino_FreeRTOS_Library
#include <PID_v1.h> //Uses PID Library, see http://playground.arduino.cc/Code/PIDLibrary
#include <math.h>
#include "pin_defs.h"

#define INCLUDE_uxTaskGetStackHighWaterMark 1

//Constants
const double max_adc = 1024.0;   //Maximum value read from ADC.
const double max_power = 6.0;    //Maximum power of the ACJ heater in kW

//calculates the temperature from a NTC thermistor from an AD converter value
double calcTempFromThermistor(int raw_adc) {
	const double rs_ntc = 34.0;     //Serial resistor value [kohm] for both NTC sensors
	const double tau = 8.4404e-7;   //used in Beta-parameter formula [kohm]
	const double beta = 5198.89;    //the Beta param for the NTC sensor, see https://en.wikipedia.org/wiki/Thermistor
	const double t0_degc = -273.15;	//absolute zero temperature in Celcius

	auto r_ntc = rs_ntc * max_adc / raw_adc - rs_ntc; //Calculate resistance from AD converter value
	auto temp = beta / log(r_ntc / tau) + t0_degc; //use beta formula to calculate temperature from resistance
	return temp;
}

// Calculates and returns the temperature setpoint from the potentiometer reading
double readSetpoint(int raw_adc) {
	const double rs_pot = 43.0; //Serial resistor value [kohm] for potentiometer

	auto r_pot = max_adc / raw_adc * rs_pot - rs_pot;
	auto temp_sp = (1 - r_pot / rs_pot) * 20 + 5;
	return temp_sp;
}

void setHeater(const double output_power, bool &relay_3kw_on, unsigned long &pwm_start_time,
		const unsigned long heater_period) {
	//ACJ output power is split in two heating element sets,
	//the first is fixed 3kW controlled by a relay
	//and the other is Triac controllable between 0 to 3kW.
	//The PID function doesn't know this and thinks it controls 0 to 6kW
	//Therefore we must recalculate the given power value depending on the status of the relay.

	double power_val;
	unsigned long output_time = 0;
	const double power_hysteresis = 300.0;

	auto half_power = static_cast<double>(heater_period) / 2.0;

	//3kW relay hysteresis
	if (output_power > (half_power + power_hysteresis)) {
		relay_3kw_on = true;
	} else if (output_power < (half_power - power_hysteresis)) {
		relay_3kw_on = false;
	} else {
		//dont change relay status
	}

	if (relay_3kw_on) {
		//turn on fixed 3kW heaters
		//and subtract those 3kW from the output power to get the remaining for the triacs
		//we need to double that value as we only controls the triacs of 3kW.
		digitalWrite(relay_pin, HIGH);
		power_val = 2 * (output_power - half_power) + 0.5;
		if (power_val < 0.0)
			power_val = 0.0;
		output_time = (unsigned long) power_val;
	} else {
		//The OutputPower is less than halfPower so we turn off the relay for the fixed 3kW.
		//But since the value of OutputPower is related to FullPower (6kW),
		//we need to double that value as we only controls the triacs 3kW.
		digitalWrite(relay_pin, LOW);
		power_val = 2 * output_power + 0.5;
		if (power_val < 0.0)
			power_val = 0.0;
		output_time = (unsigned long) power_val;
	}

	if ((millis() - pwm_start_time) > heater_period) {
		//time to shift the Relay Window
		pwm_start_time += heater_period;
	}

	//turn the triacs on/off based on pid output
	if (output_time > (millis() - pwm_start_time)) {
		digitalWrite(triac1_pin, HIGH);
		digitalWrite(triac2_pin, HIGH);
		digitalWrite(triac3_pin, HIGH);
		digitalWrite(triac_status_pin, HIGH);
	} else {
		digitalWrite(triac1_pin, LOW);
		digitalWrite(triac2_pin, LOW);
		digitalWrite(triac3_pin, LOW);
		digitalWrite(triac_status_pin, LOW);
	}
}

// Task that controls the heater power
void powerController(void* parameters) {
	UBaseType_t ux_high_water_mark = 0;
	double room_temp = 20.0;
	double desired_temp = 20.0;
	double output_power = 0.0;
	unsigned long pwm_start_time;
	const unsigned long heater_period = 10000; //Heater DutyCycle period time [ms]
	const int pid_calc_period_ms = 1000; ///do PID calculation every second.
	const TickType_t pid_loop_delay = 150 / portTICK_PERIOD_MS; // @suppress("C-Style cast instead of C++ cast")
	unsigned long last_print_time = 0;

	bool relay_3kw_on = false;
	int num_temps = 0;
	double current_room_temp = 0.0;

	double kp = 2500; // PID proportional gain
	double ki = 0.4; // PID integral time
	double kd = 0; // PID derivate time

	//Instantiate the PID object, set it's variables and initial tuning parameters
	PID my_pid(&room_temp, &output_power, &desired_temp, kp, ki, kd, DIRECT);
	//turn the PID on
	my_pid.SetOutputLimits(0, static_cast<double>(heater_period)); //tell the PID to range between 0 and the PWM-period
	my_pid.SetSampleTime(pid_calc_period_ms);
	pwm_start_time = millis();
	my_pid.SetMode(AUTOMATIC);

	for (;;) { // A Task shall never return or exit.
		//Read the actual room temperature and calculate meanvalue
		current_room_temp += calcTempFromThermistor(analogRead(room_temp_sensor_pin));
		num_temps++;
		room_temp = current_room_temp / static_cast<double>(num_temps);
		//Read the setpoint from the potentiometer
		desired_temp = readSetpoint(analogRead(potentiometer_pin));

		//calculate heating Power
		if (my_pid.Compute()) {
			current_room_temp = 0.0;
			num_temps = 0;
		}
		//set heating power
		setHeater(output_power, relay_3kw_on, pwm_start_time, heater_period);
		//print controller status to the serial link
		if ((millis() - last_print_time) > 5000) {
			last_print_time = millis();
			//print values to the tab separated columns
			Serial.print(room_temp);
			Serial.print("\t");
			Serial.print(desired_temp);
			Serial.print("\t");
			Serial.print(output_power / (double) heater_period * max_power);
			Serial.print("\t");
			Serial.print(relay_3kw_on);
			Serial.print("\r\n");
		}
		vTaskDelay(pid_loop_delay);
		ux_high_water_mark = uxTaskGetStackHighWaterMark(NULL);
	}
}

