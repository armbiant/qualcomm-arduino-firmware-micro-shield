# Coding conventions
This project shall use the following coding conventions

1. The rules explicitly defined in this document has precedence over external standards
1. Use the following standards if a specific rule or case isn’t defined in this document:
	1. [ISO CPP C++ Core Guidelines  ](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
	1. [Google C++ Style Guide](https://google.github.io/styleguide/cppguide.html)
1. Don’t reformat imported or generated code.

## Naming rules

| Type                 | Case style           |
| ---------------------| ---------------------|
| Classes & Types:     | PascalCase           |
| Enums:               | PascalCase           |
| Variables:           | snake_case           |
| Functions:           | camelCase            |
| Function parameters: | snake_case           |
| Constants:           | snake_case           |
| Macros:              | SCREAMING_SNAKE_CASE |

For function parameters, the prefix t_ is allowed.  
Type prefixes is **not** allowed.

## Comments	
Prefer //   over  /* */  
even for block comments

## Space
Line length	<= 120 characters  
Whitespace:	Never put trailing whitespace at the end of a line.  
Minimize use of vertical whitespace.

## Braces
Follow “K&R style”, e.g.:  

```
class Point {
public:
	Point(double x, double y) : x(x), y(y) {
	}
	double distance(const Point& other) const;
	double x;
	double y;
};
double Point::distance(const Point& other) const {
	double dx = x - other.x;
	double dy = y - other.y;
	return sqrt(dx * dx + dy * dy);
}
```
## Indentation
Use **tabs**, not spaces

If a function with arguments doesn’t fit into one line, do NOT over indent the second line to keep the arguments/parameters aligned with the first:  

```
bool result = DoSomething(averyveryveryverylongargument1,
                          argument2, argument3); //don't do this

Instead use a tab indent on the second and following rows:

bool result = DoSomething(averyveryveryverylongargument1,
	argument2, argument3);

```
## File names
C++ source files use .cpp  suffix  
C++ header files use .h    suffix
